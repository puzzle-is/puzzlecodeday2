﻿using System;

namespace day2puzzle4
{
    class Program
    {
        public static int Nthnumber(int n)
        {
            int count=0;
            for(int current = 1;;current++)
            {
                int sum=0;
                for(int x=current;x>0;x=x/10)
                sum=sum+x%10;

                if(sum==10)
                count++;

                if(count==n)
                return current;

            }
        }
            public static void Main()
            {
                Console.WriteLine(Nthnumber(6));
            }
            
        
    }
}
