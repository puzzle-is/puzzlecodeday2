﻿using System;

namespace day2puzzle3
{
    class Program
    {
        static int maximumproduct(int [] arr, int n)
        {
            //checking if size of array is more than three
            if(n<3)
            return -1;

            int max_product= int.MinValue;//minvalue is 0

            for (int i=0 ; i<n-2 ; i++)
            for(int j=i+1;j<n-1;j++)
            for(int k=j+1;k<n;k++)
            max_product=Math.Max(max_product,arr[i]*arr[j]*arr[k]);
            return max_product;
        }


        public static void Main ()
        {
            int []arr ={-2,2,-4,3,4};
            int n = arr.Length;
            int max = maximumproduct(arr,n);
            if(max==-1)
            Console.WriteLine("triplet not exist");
            else
            Console.WriteLine("product is" +max);
           

        
        
    }
}
}
