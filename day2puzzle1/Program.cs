﻿using System;

namespace day2puzzle1
{
    public class Program
    {
	internal static int R, C;
	internal static int[] x = new int[] {1, 0};
	internal static int[] y = new int[] {0, 1};
	internal static bool search2D(char[][] grid, int row, int column, string word)
	{
			if (grid[row][column] != word[0])
			{
				return false;
			}
			int len = word.Length;
			for (int dir = 0; dir < 2; dir++)
			{
				int k, rd = row + x[dir], cd = column + y[dir];
				for (k = 1; k < len; k++)
				{
					if (rd >= R || rd < 0 || cd >= C || cd < 0)
					{
						break;
					}
					if (grid[rd][cd] != word[k])
					{
						break;
					}
					rd =rd + x[dir];
					cd =cd + y[dir];
				}
				if (k == len)
				{
					return true;
				}
			}
		return false;
	}
	internal static void patternSearch(char[][] grid, string word)
	{
		for (int row = 0; row < R; row++)
		{
			for (int column = 0; column < C; column++)
			{
				if (search2D(grid, row, column, word))
				{
					Console.WriteLine("given pattern " + word + " found ");
				}
			}
		}
	}
	public static void Main(string[] args)
	{
			R = 4;
			C = 4;
			char[][] grid = new char[][]
			{
				new char[] {'F', 'A', 'C', 'I'},
				new char[] {'O', 'B', 'Q', 'P'},
				new char[] {'A', 'N', 'O', 'B'},
				new char[] {'M', 'A', 'S', 'S'}
			};
			patternSearch(grid, "IPBS");
			Console.WriteLine();
			patternSearch(grid, "MASS");
	}
}
    
}
