﻿using System;

namespace day2puzzle2
{
    class Program
    {
        static bool sumsubset(int[]arr ,int n,int sum)
        {
            if (sum==0)
            return true;
            if (n==0 && sum!=0)
            return false;


            if(arr[n-1]>sum)
            return sumsubset(arr,n-1,sum);

            return  sumsubset(arr,n-1,sum)||  sumsubset(arr,n-1,sum-arr[n-1]);
        }

        static bool partition(int []arr,int n)
        {
            int sum=0;
            for(int i =0;i<n;i++)
            sum = sum +arr[i];

            if(sum%2!=0)
            return false;

            return sumsubset(arr,n,sum/2);
        }

        public static void Main()
        {
            int [] arr ={5,10,20};
            int n = arr.Length;

            if(partition(arr,n)==true)
            Console.WriteLine("divided into two "+"subsets of equal sum");

            else
            Console.WriteLine("cannot divided into two "+" subsets of equal sum");

            
        }
    }
}
